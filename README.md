# cs441 Sound iOS #

Simple iOS application demonstrating triggering a sound, and also checking on acceleration (to trigger the sound based on the motion of the phone).

This is a demo program for CS441 at SUNY Binghamton.
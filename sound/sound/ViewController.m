//
//  ViewController.m
//  sound
//
//  Created by Patrick Madden on 2/4/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (assign) SystemSoundID sound;
@end

@implementation ViewController
@synthesize motionManager;
static double accelX, accelY, accelZ;

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"snap" ofType:@"aif"];
	NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
	AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &_sound);
	
	motionManager = [[CMMotionManager alloc] init];
	[motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
		float dx, dy, dz;
		CMAcceleration acceleration = [accelerometerData acceleration];
		dx = fabs(acceleration.x - accelX);
		dy = fabs(acceleration.y - accelY);
		dz = fabs(acceleration.z - accelZ);
		
		accelX = acceleration.x;
		accelY = acceleration.y;
		accelZ = acceleration.z;
		
		if ((dx + dy + dz) > 1)
		{
			[self performSelectorOnMainThread:@selector(makeSound:) withObject:NULL waitUntilDone:NO];
		}
		
	}];

}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(IBAction)makeSound:(id)sender
{
	AudioServicesPlaySystemSound(self.sound);
}
@end
